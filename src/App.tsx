import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShare } from "@fortawesome/free-solid-svg-icons";
import { faUndoAlt } from "@fortawesome/free-solid-svg-icons";
import { faSortDown } from "@fortawesome/free-solid-svg-icons";
import React, { useState } from "react";
import {
  AddSection,
  InputHeading,
  MainInput,
  MainInputContainer,
  SearchSection,
  SellContainer,
  Addcont,
  SalesActions,
  SalesActionsbtn,
  AddinputContainer,
  SearchInputCont,
  AddSectioncont,
  Products,
  AddProducts,
  ADD,
  Subtotal,
  Tex,
  ADDleft,
  ADDright,
  Subtotalleft,
  Subtotalright,
  Texleft,
  Texright,
  PayButtun,
  Paybtn,
  Paybtnleft,
  Paybtnright,
} from "./Components/styled";
import "./App.css";
import { Productaccordingly } from "./Components/Productaccordingly";

function App() {
  return (
    <>
      <div className="App">
        <SellContainer>
          <SearchSection>
            <SearchInputCont>
              <InputHeading>Search for products</InputHeading>
              <MainInputContainer className="new">
                {/* <FontAwesomeIcon className="Icon" icon={faSearch} /> */}
                {/* <i className="fa fa-Search"></i> */}
                <MainInput placeholder="Start typing or scanning..." />
              </MainInputContainer>
            </SearchInputCont>
          </SearchSection>
          <AddSection>
            <AddSectioncont>
              <SalesActions>
                <SalesActionsbtn>
                  <FontAwesomeIcon className="Icon" icon={faShare} />
                  <span>Retrieve Sale</span>
                </SalesActionsbtn>
                <SalesActionsbtn>
                  <FontAwesomeIcon className="Icon" icon={faUndoAlt} />
                  <span>Park Sale</span>
                </SalesActionsbtn>
                <SalesActionsbtn>
                  <FontAwesomeIcon className="" icon={faSortDown} />
                  <span>More Actions...</span>
                </SalesActionsbtn>
              </SalesActions>
              <Addcont>
                <AddinputContainer>
                  <MainInput
                    max={"4px"}
                    width={"90%"}
                    placeholder="Start typing or scanning..."
                  />
                </AddinputContainer>
              </Addcont>
              <Products>
                <Productaccordingly />
                <Productaccordingly />
                <Productaccordingly />
              </Products>
              <AddProducts>
                <ADD>
                  <ADDleft>
                    <h4>ADD</h4>
                  </ADDleft>
                  <ADDright>
                    <span>Discount</span>
                    <span>Promo Code</span>
                    <span>Discount</span>
                  </ADDright>
                </ADD>
                <Subtotal>
                  <Subtotalleft>Subtotal</Subtotalleft>
                  <Subtotalright>000.00</Subtotalright>
                </Subtotal>
                <Tex>
                  <Texleft>
                    <span>Tex</span>
                    <span>No Tex</span>
                  </Texleft>
                  <Texright>000.00</Texright>
                </Tex>
                <PayButtun>
                  <Paybtn>
                    <Paybtnleft>
                      <h3>Pay</h3>
                      <span>00 items</span>
                    </Paybtnleft>
                    <Paybtnright>PKRs0000.00</Paybtnright>
                  </Paybtn>
                </PayButtun>
              </AddProducts>
            </AddSectioncont>
          </AddSection>
        </SellContainer>
      </div>
    </>
  );
}

export default App;
