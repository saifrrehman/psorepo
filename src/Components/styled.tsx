import styled from "styled-components";
import user from ".././assets/user-solid.svg";

import icon from ".././assets/search-solid.svg";
const SellContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  align-content: center;
  width: 100%;
  height: 100vh;
  background-color: #f4f2f5;
`;
const SearchSection = styled.div`
  width: 60%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: end;
  align-content: center;
`;

const InputHeading = styled.div`
  font-family: lato, Helvetica Neue, Helvetica, Roboto, Arial, sans-serif;
  font-weight: 700;
  font-size: 15px;
  line-height: 20px;
  color: #27252a;
  font-style: normal;
  top: 91px;
  left: 101px;
  padding-left: 4px;
  padding-top: 109px;
  padding-bottom: 5px;
  align-self: flex-start;
`;
const MainInputContainer = styled.div`
  background-color: #ffffff;
  position: relative;
  width: 100%;
  ::after {
    content: "";
    width: 14px;
    height: 14px;
    background-image: url(${icon});
    position: absolute;
    z-index: 1;
    left: 8px;
    top: 13px;
  }
`;
const SearchInputCont = styled.div`
  width: 75%;
  /* margin: 0 auto; */
  padding-right: 20px;
  padding-left: 100px;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: flex-end;
`;

const MainInput = styled.input`
  width: 96%;
  height: 36px;
  outline: none;
  border: none;
  padding-left: 10px;
  border-radius: 3px;
  padding-left: 30px;
  border: 2px solid #e7e5e8;
  width: ${(props) => (props.width ? "90%" : "")};
  margin: ${(props) => (props.max ? "4px" : "")};
  ::placeholder {
    color: #32577d;
    font-size: 15px;
  }
  &:focus {
    border-color: #3f32f5;
    box-shadow: 0 0 3px #3f32f5;
  }
`;
const AddSection = styled.div`
  width: 40%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: baseline;
  align-content: center;
`;
const AddSectioncont = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: flex-start;
  align-content: stretch;
`;

const SalesActions = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  padding-top: 81px;
  padding-bottom: 10px;
`;
const SalesActionsbtn = styled.button`
  height: 43px;
  opacity: 0.35;
  outline: none;
  border: none;
  font-size: 18px;
  align-items: center;
  margin-right: 15px;
  border-radius: 5px;
  &:hover {
    color: #ffffff;
    background-color: #32577d;
  }
  span {
    font-size: 16px !important;
    padding-left: 17px;
  }
`;
const Addcont = styled.div`
  /* width: auto; */
  width: 75%;
  /* width: fit-content; */
  position: relative;
  border: 1px solid lightgrey;
`;
const AddinputContainer = styled.div`
  width: 100%;
  height: 49px;
  background-color: #ffffff;
  ::after {
    content: "";
    width: 14px;
    height: 14px;
    background-image: url(${user});
    background-repeat: no-repeat;
    position: absolute;
    z-index: 1;
    left: 13px;
    top: 16px;
  }
`;

// ------------------------products--------------
const Products = styled.div`
  width: 75%;
  height: 280px;
  overflow-x: hidden;
  overflow-y: auto;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  background-color: #ffffff;
`;
const Productcontainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  transition: 1s all;
  border: 1px solid lightgrey;
`;
const ProductHeader = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  font-size: 17px;
  width: 100%;
  font-family: lato, Helvetica Neue, Helvetica, Roboto, Arial, sans-serif;
  cursor: pointer;
`;
const ProductHeaderleft = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  span {
    padding: 10px;
  }
`;
const ProductHeaderright = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-end;
  align-items: center;
  align-content: center;
  span {
    padding: 0px 30px;
  }
`;
const ProductContent = styled.div`
  width: 100%;
  display: none;
  &.show {
    display: block;
  }
`;
const ProductDetails = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
`;
const DeatialsSet = styled.div`
  width: 33%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;
const SetsOptions = styled.p`
  align-self: flex-start;
  margin: 6px 0px 8px 23px;
`;
const DeatialsInput = styled.input`
  width: 110px;
  height: 36px;
  outline: none;
  border: 2px solid #e7e5e8;
  border-radius: 5px;
  &:focus {
    border-color: #3f32f5;
    box-shadow: 0 0 3px #3f32f5;
  }
`;
const NoteContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: flex-start;
  span {
    color: #3f32f5;
    font-size: 12px;
    font-weight: 400;
    align-self: flex-end;
    padding: 5px;
    padding-right: 20px;
    :hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }
`;
const Noteinput = styled.input`
  width: 90%;
  height: 36px;
  outline: none;
  border: 2px solid #e7e5e8;
  border-radius: 5px;
  margin-left: 20px;
  padding-left: 10px;
  &:focus {
    border-color: #3f32f5;
    box-shadow: 0 0 3px #3f32f5;
  }
`;
const AddProducts = styled.div`
  /* height: 200px; */
  width: 75%;
  background-color: white;
`;
const ADD = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: flex-start;
`;
const ADDleft = styled.div`
  h4 {
    margin: 0;
    padding: 13px 17px;
  }
`;
const ADDright = styled.div`
  span {
    padding: 7px;
    font-size: 15px;
    color: #3f32f5;
    font-family: lato, Helvetica Neue, Helvetica, Roboto, Arial, sans-serif;
    font-weight: 600;
    cursor: pointer;
  }
`;
const Subtotal = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: flex-start;
`;
const Subtotalleft = styled.div`
  padding: 13px 17px;
  font-family: lato, Helvetica Neue, Helvetica, Roboto, Arial, sans-serif;
  margin: 0;
`;
const Subtotalright = styled.div`
  padding: 8px 17px;
`;
const Tex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: flex-start;
`;
const Texleft = styled.div`
  margin: 0;
  span {
    padding: 10px 17px;
    margin: 0;
  }
`;
const Texright = styled.div`
  padding: 13px 17px;
`;
const PayButtun = styled.div`
  width: 100%;
  height: 95px;
  background-color: #e7e5e8;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: flex-start;
`;
const Paybtn = styled.div`
  width: 80%;
  border-radius: 3px;
  height: 60px;
  background-color: #3f32f5;
  color: #fff;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  :hover {
    opacity: 0.7;
    cursor: pointer;
  }
`;
const Paybtnleft = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  h3 {
    margin: 0;
    font-size: 22px;
    padding-left: 10px;
    font-weight: 400;
  }
  span {
    font-size: 12px;
    align-self: flex-end;
    padding-left: 10px;
  }
`;
const Paybtnright = styled.div`
  padding-right: 15px;
  font-size: 22px;
`;

export {
  InputHeading,
  MainInput,
  MainInputContainer,
  SellContainer,
  SearchSection,
  AddSection,
  Addcont,
  SalesActions,
  SalesActionsbtn,
  AddinputContainer,
  SearchInputCont,
  AddSectioncont,
  Products,
  Productcontainer,
  ProductHeader,
  ProductContent,
  ProductHeaderleft,
  ProductHeaderright,
  ProductDetails,
  DeatialsSet,
  SetsOptions,
  DeatialsInput,
  NoteContainer,
  Noteinput,
  AddProducts,
  ADD,
  ADDleft,
  ADDright,
  Subtotal,
  Subtotalleft,
  Subtotalright,
  Tex,
  Texleft,
  Texright,
  PayButtun,
  Paybtn,
  Paybtnleft,
  Paybtnright,
};
