import { faChevronRight, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import {
  DeatialsInput,
  DeatialsSet,
  NoteContainer,
  Noteinput,
  Productcontainer,
  ProductContent,
  ProductDetails,
  ProductHeader,
  ProductHeaderleft,
  ProductHeaderright,
  SetsOptions,
} from "./styled";

export const Productaccordingly = () => {
  const [showDetails, setshowDetails] = useState(false);
  return (
    <div>
      <Productcontainer>
        <ProductHeader
          onClick={() => {
            setshowDetails(!showDetails);

            console.log(showDetails);
          }}
        >
          <ProductHeaderleft>
            <FontAwesomeIcon
              className={showDetails === true ? "trans" : "ChevronRight"}
              icon={faChevronRight}
            />
            <span>0</span>
            <p>Product-1</p>
          </ProductHeaderleft>
          <ProductHeaderright>
            <span>00.00</span>

            <FontAwesomeIcon className="TrashAlt" icon={faTrashAlt} />
          </ProductHeaderright>
        </ProductHeader>
        <ProductContent className={showDetails === true ? "show" : ""}>
          <ProductDetails>
            <DeatialsSet>
              <SetsOptions>Quantity</SetsOptions>
              <DeatialsInput />
            </DeatialsSet>
            <DeatialsSet>
              <SetsOptions>Price</SetsOptions>
              <DeatialsInput />
            </DeatialsSet>
            <DeatialsSet>
              <SetsOptions>Discount(%)</SetsOptions>
              <DeatialsInput />
            </DeatialsSet>
          </ProductDetails>
          <NoteContainer>
            <SetsOptions>Notes</SetsOptions>
            <Noteinput placeholder="Type to add note..." />
            <span
            // onClick={() => {
            //   setshowDetails(false);
            // }}
            >
              Show Inventery & Details
            </span>
          </NoteContainer>
        </ProductContent>
      </Productcontainer>
    </div>
  );
};
